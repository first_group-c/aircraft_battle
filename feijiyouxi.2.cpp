#include<stdio.h>
#include<stdlib.h>
#include<windows.h>
#include<conio.h>
int position_x,position_y;//位置 
int wide,high;//区域 
int bullet_x,bullet_y;//子弹位置 
int enemy_x,enemy_y;//敌机位置
int score;//得分 
int victor;
void HideCusor()
{
 CONSOLE_CURSOR_INFO cursor_info={1,0};//第二个值为0，表示隐藏光标
 SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&cursor_info); 
} 
void gotoxy(int x,int y)//光标移动 
{
 HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
 COORD pos;
 pos.X=x;
 pos.Y=y;
 SetConsoleCursorPosition(handle,pos);
}
void startup()//初始化 
{
 wide=30;
 high=20;
 position_x=wide/2;
 position_y=high-3;
 bullet_x=-1;
 bullet_y=-1;
 enemy_x=5;
 enemy_y=0;
 score=0;
 victor=1;
 HideCusor();
}
void show()
{
 gotoxy(0,0);//光标至原点 
 int i,j;//行计数，列计数 
 for(i=0;i<high;i++)
 {
  for(j=0;j<wide;j++)
  {
   if((i==position_y)&&(j==position_x))
    printf("*");
   else if((i==enemy_y)&&(j==enemy_x))//敌机 
    printf("$");
   else if((i==bullet_y)&&(j==bullet_x))//子弹
    printf("+");
   else
    printf(" ");
   if((enemy_x==position_x)&&(enemy_y==position_y))//判断胜负 
   {
    enemy_y=-1;
    victor=0;
   }
  }
  printf("\n");   
 }
  printf("得分：%d\n",score);
}
void UpdateWithoutInput()
{
 if(bullet_y>-1)
  bullet_y--;
 if((enemy_y==bullet_y)&&(enemy_x==bullet_x))//计分 
 {
  score+=10;
  printf("\a"); 
  enemy_y=0;
  enemy_x=rand()%31;
  bullet_x=-1;
 }
 
 static int speed=0;//减慢$下降的速度 
 if(speed<20)
  speed++;
 if(speed==20)
 {
  if(enemy_y>high) 
  {
   enemy_x=rand()%31;
   enemy_y=0;
  } 
  else
  {
   enemy_y++;
   speed=0;
  }
 }
}
void UpdateWithInput()
{
 char input;
 if(kbhit())
 {
  input=getch();//输入 
  if(input=='w')
   position_y--;
  if(input=='s')
   position_y++;
  if(input=='a')
   position_x--;
  if(input=='d')
   position_x++;
  if(input==' ')
  {
   bullet_y=position_y-1;
   bullet_x=position_x;
  }
 }
}
int main()
{
 startup();
 while(victor)
 {
  show();
  UpdateWithoutInput();
  UpdateWithInput();
 }
 printf("GAME OVER!\nhaha\n");
 return 0;
}

