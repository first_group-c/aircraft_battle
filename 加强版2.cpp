#include <graphics.h>
#include <conio.h>
#include <math.h>
#pragma comment(lib, "Winmm.lib")


void startup(); //数据的初始化
void show(); //显示画面
void updateWithoutInput(); //与用户无关的更新
void updateWithInput(); //与用户有关的更新
void gameover(); //游戏结束，进行后续处理


#define high 675 //画布的高度
#define width 900 //画布的宽度
#define my_plane_high 133 //我方飞机的高度
#define my_plane_width 99 //我方飞机的宽度
#define enemy1_plane_high 50 //敌方飞机1的高度
#define enemy1_plane_width 68 //敌方飞机2的宽度
#define my_bullet1_high 20 //我方子弹1的高度
#define my_bullet1_width 20 //我方子弹1的宽度
#define enemy_bullet1_high 20 //敌方子弹1的高度
#define enemy_bullet1_width 20 //地方子弹1的宽度
#define bomb_high 64 //飞机爆炸画面的高度
#define bomb_width 64 //飞机爆炸画面的宽度
#define enemy_planeNum 5 //敌机个数


int canvas[high][width] = { 0 }; //定义地图数组，用于存放子弹等,1为我方1级子弹，-1为敌机1级子弹
int boom_i = 0; //定义用来显示爆炸过程


IMAGE img_bk; //定义背景图片
IMAGE img_my_plane1, img_my_plane2; //定义我方飞机图片,1黑白，2彩色
IMAGE img_enemy_plane1, img_enemy_plane2; //定义敌方飞机1,1黑白，2彩色
IMAGE img_my_bullet11, img_my_bullet12; //定义我方子弹1,1黑白，2彩色
IMAGE img_enemy_bullet11, img_enemy_bullet12;  //定义敌方子弹1,1黑白，2彩色
IMAGE img_boom1, img_boom2; //定义敌方飞机爆炸，1黑白，2彩色

typedef struct plane //定义结构体用于人
{
float left_x;  //左端x坐标
float left_y;  //左端y坐标
float right_x;//右端x坐标
float right_y;//右端y坐标
float top_x;  //上端x坐标
float top_y;  //上端y坐标
float bottom_x;//下端x坐标
float bottom_y;//下端y坐标
float center_x;//中心x坐标
float center_y;//中心y坐标
int bullet;  //定义子弹种类
int HP;  //定义剩余血量
int isFire;  //定义敌机是否开枪
}; 
plane my_plane; //定义我方飞机
plane enemy_plane[enemy_planeNum]; //定义敌方飞机


void main()
{
startup();
while (1)
{
show();
updateWithoutInput();
updateWithInput();
}
gameover();
}


void startup()
{
initgraph(width, high);//初始化图形窗口
/*导入图片*/
loadimage(&img_bk, _T("background.jpg"));
loadimage(&img_my_plane1, _T("my_plane_1.jpg"));
loadimage(&img_my_plane2, _T("my_plane_2.jpg"));
loadimage(&img_enemy_plane1, _T("enemy_plane_1.jpg"));
loadimage(&img_enemy_plane2, _T("enemy_plane_2.jpg"));
loadimage(&img_my_bullet11, _T("bullet_11.jpg"));
loadimage(&img_my_bullet12, _T("bullet_12.jpg"));
loadimage(&img_enemy_bullet11, _T("bullet_11.jpg"));
loadimage(&img_enemy_bullet12, _T("enemy_bullet_12.jpg"));
loadimage(&img_boom1, _T("bwbomb_boom.jpg"));
loadimage(&img_boom2, _T("bomb_boom.jpg"));
/*初始化我方飞机,每个坐标都与中心坐标相关联，增强坐标关联性*/
my_plane.center_x = width / 2;
my_plane.center_y = high / 4 * 3;
my_plane.left_x = my_plane.center_x - my_plane_width / 2;
my_plane.left_y = my_plane.center_y;
my_plane.right_x = my_plane.center_x + my_plane_width / 2;
my_plane.right_y = my_plane.center_y;
my_plane.top_x = my_plane.center_x;
my_plane.top_y = my_plane.center_y - my_plane_high / 2;
my_plane.bottom_x = my_plane.center_x;
my_plane.bottom_y = my_plane.center_y + my_plane_high / 2;
my_plane.bullet = 1;
my_plane.HP = 3;
/*初始化敌机*/
for (int k = 0; k < enemy_planeNum; k++)
{
enemy_plane[k].center_x = rand() % 850 + 50;
enemy_plane[k].center_y = rand() % 2 - 20 * k;
enemy_plane[k].left_x = enemy_plane[0].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[0].center_y;
enemy_plane[k].right_x = enemy_plane[0].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[0].center_y;
enemy_plane[k].top_x = enemy_plane[0].center_x;
enemy_plane[k].top_y = enemy_plane[0].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[0].center_x;
enemy_plane[k].bottom_y = enemy_plane[0].center_y + enemy1_plane_high / 2;
enemy_plane[k].bullet = 1;
enemy_plane[k].HP = 1;
enemy_plane[k].isFire = rand() % 30;
}
BeginBatchDraw();
}


void show()
{
int i, j;
putimage(0, 0, &img_bk);//定义背景图
if (my_plane.HP > 0)//打印我方飞机
{
putimage(my_plane.left_x, my_plane.top_y, &img_my_plane1, NOTSRCERASE);//我方飞机，黑白
putimage(my_plane.left_x, my_plane.top_y, &img_my_plane2, SRCINVERT);//我方飞机，彩色
}
for (int k = 0; k < enemy_planeNum; k++)//打印敌方飞机
{
if (enemy_plane[k].HP > 0)
{
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_enemy_plane1, NOTSRCERASE);//敌机，黑白
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_enemy_plane2, SRCINVERT);//敌机，彩色
}
else if (enemy_plane[k].HP == 0)//敌方飞机HP == 0时爆炸，产生新的敌机
{
for (boom_i = 0; boom_i <= 8; boom_i++)
{
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, bomb_width, bomb_high, &img_boom1, boom_i * bomb_width, 0, NOTSRCERASE);//放置飞机爆炸黑白图片
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, bomb_width, bomb_high, &img_boom2, boom_i * bomb_width, 0, SRCINVERT);//放置飞机爆炸的彩色图片
}
enemy_plane[k].center_x = rand() % 750 + 34 * k;
enemy_plane[k].center_y = -enemy1_plane_high / 4;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
enemy_plane[k].bullet = 1;
enemy_plane[k].HP = 2;
enemy_plane[k].isFire = rand() % 30;
}
}
/*打印子弹*/
for (i = 0; i < high; i++)
{
for (j = 0; j < width; j++)
{
if (canvas[i][j] == 1)//打印我机子弹，1黑白，2彩色
{
putimage(j - 10, i, &img_my_bullet11, NOTSRCERASE);
putimage(j - 10, i, &img_my_bullet12, SRCINVERT);
}
else if (canvas[i][j] == -1)//打印敌机子弹，1黑白，2彩色
{
putimage(j - 10, i, &img_enemy_bullet11, NOTSRCERASE);
putimage(j - 10, i, &img_enemy_bullet12, SRCINVERT);
}
}
}
FlushBatchDraw();
Sleep(2);
}


void updateWithoutInput()
{
/*我机子弹移动*/
for (int k = 0; k < enemy_planeNum; k++)
{
for (i = 0; i < high; i++)
{
for (j = 0; j < width; j++)
{
if (canvas[i][j] == 1)
{
/*子弹击中敌机*/
if (enemy_plane[k].HP > 0)
{
if (i <= enemy_plane[k].bottom_y && j >= enemy_plane[k].left_x && j <= enemy_plane[k].right_x && my_plane.top_y > enemy_plane[k].bottom_y)
{
enemy_plane[k].HP = enemy_plane[k].HP - my_plane.bullet;
canvas[i][j] = 0;
}
}
/*子弹上移*/
int temp = canvas[i][j];
canvas[i][j] = 0;
if (i > 5)
canvas[i - 3][j] = temp;
}
}
}
}


for (int k = 0; k < enemy_planeNum; k++)
{
enemy_plane[k].isFire = rand() % 80;//敌机随机发射子弹，0为不发射，1为发射
/*敌机发射子弹,且HP>0*/
if (enemy_plane[k].HP >= 0 && enemy_plane[k].bottom_y > 15)
{
if (enemy_plane[k].isFire == 1)
{
enemy_plane[k].bullet = 1;
int m, n;
m = enemy_plane[k].bottom_y;
n = enemy_plane[k].bottom_x;
canvas[m][n] = -1;
}
}
}

/*敌机子弹移动*/
for (i = high - 1; i > 0; i--)
{
for (j = width - 1; j > 0; j--)
{
/*子弹下移*/
if (canvas[i][j] == -1)
{
/*子弹击中我机*/
if (abs(my_plane.center_y - i) <= my_plane_high / 2 && j >= my_plane.left_x && j <= my_plane.right_x && my_plane.HP > 0)
{
my_plane.HP = my_plane.HP - enemy_plane[0].bullet;
canvas[i][j] = 0;
}
/*子弹下移*/
int temp = canvas[i][j];
canvas[i][j] = 0;
if (i < high - 2)
canvas[i + 4][j] = temp;
}
}
}
/*敌机撞到我机*/
for (int k = 0; k < enemy_planeNum; k++)
{
if (abs(my_plane.center_y - enemy_plane[k].center_y) <= my_plane_high / 2 + enemy1_plane_high / 2 - 20)
{
if (enemy_plane[k].left_x <= my_plane.right_x && enemy_plane[k].right_x >= my_plane.left_x && my_plane.HP > 0)
my_plane.HP = 0;
}
/*敌机向下移动*/
if (enemy_plane[k].HP >= 0)
{
if (enemy_plane[k].top_y <= high)
{
enemy_plane[k].center_y = enemy_plane[k].center_y + 1.5;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
}
/*敌机飞出屏幕，重置新的敌机*/
else
{
enemy_plane[k].center_x = rand() % 750 + 34 * k;
enemy_plane[k].center_y = -enemy1_plane_high / 4;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
enemy_plane[k].bullet = 1;
enemy_plane[k].HP = 2;
enemy_plane[k].isFire = rand() % 3;
}
}
}

}


void updateWithInput()
{
MOUSEMSG m;  
while (MouseHit())
{
m = GetMouseMsg();
/*飞机的位置等于鼠标所在的位置*/
if (m.uMsg == WM_MOUSEMOVE && my_plane.HP > 0)
{
my_plane.center_x = m.x;
my_plane.center_y = m.y;
my_plane.left_x = my_plane.center_x - my_plane_width / 2;
my_plane.left_y = my_plane.center_y;
my_plane.right_x = my_plane.center_x + my_plane_width / 2;
my_plane.right_y = my_plane.center_y;
my_plane.top_x = my_plane.center_x;
my_plane.top_y = my_plane.center_y - my_plane_high / 2;
my_plane.bottom_x = my_plane.center_x;
my_plane.bottom_y = my_plane.center_y + my_plane_high / 2;
}
/*发射子弹*/
else if (m.uMsg == WM_LBUTTONDOWN && my_plane.HP > 0)
{
int m, n;
m = my_plane.top_y;
n = my_plane.top_x;
canvas[m][n] = 1;//canvas数组中1代表我方1号子弹
}
}
}


void gameover()
{
EndBatchDraw();
_getch();
closegraph();
}

