#include <graphics.h>
#include <conio.h>
#include <math.h>
#include <stdio.h>
#pragma comment(lib, "Winmm.lib")


void printBullet(); //打印子弹
void startup(); //数据的初始化
void show(); //显示画面
void updateWithoutInput(); //与用户无关的更新
void updateWithInput(); //与用户有关的更新
void startMenu(); //游戏初始菜单界面
void pauseMenu(); //游戏暂停菜单界面，按Esc进入
void gameover(); //游戏结束，进行后续处理


#define high 675 //画布的高度
#define width 900 //画布的宽度
#define my_plane_high 133 //我方飞机的高度
#define my_plane_width 99 //我方飞机的宽度
#define enemy1_plane_high 50 //敌方飞机1的高度
#define enemy1_plane_width 68 //敌方飞机1的宽度
#define boss_high 169 //boss的高度
#define boss_width 203 //boss的宽度
#define my_bullet1_high 20 //我方子弹1的高度
#define my_bullet1_width 20 //我方子弹1的宽度
#define my_bullet2_high 30 //我方子弹2的高度
#define my_bullet2_width 44 //我方子弹2的宽度
#define enemy_bullet1_high 20 //敌方子弹1的高度
#define enemy_bullet1_width 20 //地方子弹1的宽度
#define boss_bullet1_high 439 //boss子弹1的高度
#define boss_bullet1_width 140 //boss子弹1的宽度
#define bomb_high 64 //飞机爆炸画面的高度
#define bomb_width 64 //飞机爆炸画面的宽度
#define enemy_planeNum 5 //敌机个数
#define reward_high 20 //奖励的高度
#define reward_width 20 //奖励的宽度


int canvas[high][width] = { 0 }; //定义地图数组，用于存放子弹等,1为我方1级子弹，-1为敌机1级子弹
int boom_i; //定义用来显示爆炸过程
float HP_i, HP_j; //用来显示HP
int score; //用来计分
int scene; //用来标识场景，scene = 
1为第一关，scene = 2为boss
int mybullet; //用来识别我方飞机发射一号子弹还是二号子弹,0为1号子弹，1为二号子弹
int enemybullet; //用来识别敌机boss的子弹类型


IMAGE img_bk; //定义背景图片
IMAGE img_startMenu; //定义初始图片
IMAGE img_pauseMenu; //定义暂停界面图片
IMAGE img_my_plane1, img_my_plane2; //定义我方飞机图片,1黑白，2彩色
IMAGE img_enemy1_plane1, img_enemy1_plane2;  //定义敌方飞机1,1黑白，2彩色
IMAGE img_enemy2_plane1, img_enemy2_plane2;  //定义敌方飞机2,1黑白，2彩色
IMAGE img_enemy3_plane1, img_enemy3_plane2;  //定义地方飞机3,1黑白，2彩色，打爆后可获得奖励
IMAGE img_boss1, img_boss2; //定义boss图片,1为黑白，2彩色
IMAGE img_my_bullet11, img_my_bullet12; //定义我方子弹1,1黑白，2彩色
IMAGE img_my_bullet21, img_my_bullet22; //定义我方子弹2,1黑白，2彩色
IMAGE img_enemy_bullet11, img_enemy_bullet12;  //定义敌方子弹1,1黑白，2彩色
IMAGE img_enemy_bullet21, img_enemy_bullet22;  //定义敌方子弹2,1黑白，2彩色
IMAGE img_bossbullet11, img_bossbullet12; //定义boss子弹1,1黑白，2彩色
IMAGE img_reward1_1, img_reward1_2; //定义奖励1，吃了后变换子弹，击中敌机HP-2
IMAGE img_boom1, img_boom2; //定义敌方飞机爆炸，1黑白，2彩色
IMAGE img_HP; //定义我方HP


typedef struct plane //定义结构体用于飞机
{
float left_x;  //左端x坐标
float left_y;  //左端y坐标
float right_x;//右端x坐标
float right_y;//右端y坐标
float top_x;  //上端x坐标
float top_y;  //上端y坐标
float bottom_x;//下端x坐标
float bottom_y;//下端y坐标
float center_x;//中心x坐标
float center_y;//中心y坐标
int bullet;  //定义子弹种类,子弹种类不同敌机也不同，
//如果0<=bullet<=16，则子弹种类为第一种，击中我机HP-1，若17<=bullet<=18，则子弹种类为
//第二种，击中我机，HP-2，若bullet=19，则为第三种敌机，不可发射子弹，且击落可获得新子弹
int HP;  //定义剩余血量
int isFire;  //定义敌机是否开枪
};
plane my_plane; //定义我方飞机
plane enemy_plane[enemy_planeNum]; //定义敌方飞机
plane boss; //定义boss


void main()
{
startup();
while (1)
{
show();
updateWithoutInput();
updateWithInput();
}
gameover();
}


void printBullet()
{
int i, j;
for (i = 0; i < high; i++)
{
for (j = 0; j < width; j++)
{
if (canvas[i][j] == 1)//打印我机子弹，1黑白，2彩色
{
putimage(j - 10, i, &img_my_bullet11, NOTSRCERASE);
putimage(j - 10, i, &img_my_bullet12, SRCINVERT);
}
if (canvas[i][j] == 2)//打印我机子弹2，1黑白，2彩色
{
putimage(j - 10, i, &img_my_bullet21, NOTSRCERASE);
putimage(j - 10, i, &img_my_bullet22, SRCINVERT);
}
else if (canvas[i][j] == -1)//打印敌机1子弹，1黑白，2彩色
{
putimage(j - 10, i, &img_enemy_bullet11, NOTSRCERASE);
putimage(j - 10, i, &img_enemy_bullet12, SRCINVERT);
}
else if (canvas[i][j] == -2)//打印敌机2子弹，1黑白，2彩色
{
putimage(j, i, &img_enemy_bullet21, NOTSRCERASE);
putimage(j, i, &img_enemy_bullet22, SRCINVERT);
}


if (canvas[i][j] <= -3 && canvas[i][j] >= -5)//打印boss子弹
{
putimage(j - 10, i, &img_enemy_bullet11, NOTSRCERASE);
putimage(j - 10, i, &img_enemy_bullet12, SRCINVERT);
}
if (canvas[i][j] == -6)
{
putimage(j, i, &img_enemy_bullet11, NOTSRCERASE);
putimage(j, i, &img_enemy_bullet12, SRCINVERT);
}
}
}
}
void startup()
{
initgraph(width, high);//初始化图形窗口
mciSendString("open .\\energyfull.mp3 alias beginmusic", NULL, 0, NULL);
mciSendString("play beginmusic", NULL, 0, NULL);//近播放一次
mciSendString("open .\\game_music.mp3 alias bkmusic", NULL, 0, NULL);
mciSendString("play bkmusic repeat", NULL, 0, NULL);//循环播放
/*导入图片*/
loadimage(&img_bk, ".\\background.jpg");
loadimage(&img_startMenu, ".\\startMenu.jpg");
loadimage(&img_pauseMenu, ".\\background.jpg");
loadimage(&img_my_plane1, ".\\my_plane_1.jpg");
loadimage(&img_my_plane2, ".\\my_plane_2.jpg");
loadimage(&img_HP, ".\\HP.jpg");
loadimage(&img_enemy1_plane1, ".\\enemy_plane1_1.jpg");
loadimage(&img_enemy1_plane2, ".\\enemy_plane1_2.jpg");
loadimage(&img_enemy2_plane1, ".\\enemy_plane2_1.jpg");
loadimage(&img_enemy2_plane2, ".\\enemy_plane2_2.jpg");
loadimage(&img_enemy3_plane1, ".\\enemy_plane3_1.jpg");
loadimage(&img_enemy3_plane2, ".\\enemy_plane3_2.jpg");
loadimage(&img_boss1, ".\\Boss1.jpg");
loadimage(&img_boss2, ".\\Boss2.jpg");
loadimage(&img_my_bullet11, ".\\bullet_11.jpg");
loadimage(&img_my_bullet12, ".\\bullet_12.jpg");
loadimage(&img_my_bullet21, ".\\bullet_21.jpg");
loadimage(&img_my_bullet22, ".\\bullet_22.jpg");
loadimage(&img_enemy_bullet11, ".\\bullet_11.jpg");
loadimage(&img_enemy_bullet12, ".\\enemy_bullet_12.jpg");
loadimage(&img_enemy_bullet21, ".\\enemy_bullet_21.jpg");
loadimage(&img_enemy_bullet22, ".\\enemy_bullet_22.jpg");
loadimage(&img_bossbullet11, ".\\Bossbullet1_1.jpg");
loadimage(&img_bossbullet12, ".\\Bossbullet1_2.jpg");
loadimage(&img_reward1_1, ".\\rewadr1_1.jpg");
loadimage(&img_reward1_2, ".\\rewadr1_2.jpg");
loadimage(&img_boom1, ".\\bwbomb_boom.jpg");
loadimage(&img_boom2, ".\\bomb_boom.jpg");
/*初始化我方飞机,每个坐标都与中心坐标相关联，增强坐标关联性*/
my_plane.center_x = width / 2;
my_plane.center_y = high / 4 * 3;
my_plane.left_x = my_plane.center_x - my_plane_width / 2;
my_plane.left_y = my_plane.center_y;
my_plane.right_x = my_plane.center_x + my_plane_width / 2;
my_plane.right_y = my_plane.center_y;
my_plane.top_x = my_plane.center_x;
my_plane.top_y = my_plane.center_y - my_plane_high / 2;
my_plane.bottom_x = my_plane.center_x;
my_plane.bottom_y = my_plane.center_y + my_plane_high / 2;
my_plane.bullet = 1;
my_plane.HP = 3;
/*初始化敌机*/
for (int k = 0; k < enemy_planeNum; k++)
{
enemy_plane[k].center_x = rand() % 850 + 50;
enemy_plane[k].center_y = rand() % 2 - 20 * k;
enemy_plane[k].left_x = enemy_plane[0].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[0].center_y;
enemy_plane[k].right_x = enemy_plane[0].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[0].center_y;
enemy_plane[k].top_x = enemy_plane[0].center_x;
enemy_plane[k].top_y = enemy_plane[0].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[0].center_x;
enemy_plane[k].bottom_y = enemy_plane[0].center_y + enemy1_plane_high / 2;
enemy_plane[k].bullet = rand() % 20;
if (enemy_plane[k].bullet >= 0 && enemy_plane[k].bullet <= 16)
enemy_plane[k].HP = 2;
if (enemy_plane[k].bullet >= 17 && enemy_plane[k].bullet <= 18)
enemy_plane[k].HP = 4;
if (enemy_plane[k].bullet == 19)
enemy_plane[k].HP = 1;
enemy_plane[k].isFire = rand() % 30;
}
/*初始化boss*/
boss.center_x = width / 2;
boss.center_y = high / 10;
boss.left_x = boss.center_x - boss_width / 2;
boss.left_y = boss.center_y;
boss.right_x = boss.center_x + boss_width / 2;
boss.right_y = boss.center_y;
boss.top_x = boss.center_x;
boss.top_y = boss.center_y - boss_high / 2;
boss.bottom_x = boss.center_x;
boss.bottom_y = boss.center_y + boss_high / 2;
boss.HP = 30;
boss.isFire = 1;
boss.bullet = 1;


boom_i = 0;
score = 0;
scene = -2;
HP_i = 120;
HP_j = 19;
mybullet = 0;


BeginBatchDraw();
}


void show()
{
int i, j;
while (scene == -2)
{
startMenu();
}
while (scene == -1)
{
pauseMenu();
}
if (score >= 200 && scene == 1)
{
scene = 2;
for (i = 0; i < high; i++)
{
for (j = 0; j < width; j++)
{
canvas[i][j] = 0;
}
}
}
putimage(0, 0, &img_bk);//定义背景图
if (my_plane.HP > 0)//打印我方飞机
{
putimage(my_plane.left_x, my_plane.top_y, &img_my_plane1, NOTSRCERASE);//我方飞机，黑白
putimage(my_plane.left_x, my_plane.top_y, &img_my_plane2, SRCINVERT);//我方飞机，彩色
}
else if (my_plane.HP <= 0)
{
settextcolor(WHITE);//设置字体为黑色
setbkmode(TRANSPARENT);//背景透明
settextstyle(50, 0, _T("黑体"));
outtextxy(width * 0.4, high * 0.4, "DEFEAT");
settextstyle(25, 0, _T("黑体"));
outtextxy(width * 0.7, high * 0.7, "制作人：");
outtextxy(width * 0.7, high * 0.8, "王俊翔");
outtextxy(width * 0.7, high * 0.85, "郭源恒");
}
if (scene == 1)
{
for (int k = 0; k < enemy_planeNum; k++)//打印敌方飞机
{
if (enemy_plane[k].bullet >= 0 && enemy_plane[k].bullet <= 16)//打印第一种敌机
{
if (enemy_plane[k].HP > 0)
{
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_enemy1_plane1, NOTSRCERASE);//敌机，黑白
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_enemy1_plane2, SRCINVERT);//敌机，彩色
}
else if (enemy_plane[k].HP <= 0)//敌方飞机HP <= 0时爆炸，产生新的敌机
{
for (boom_i = 0; boom_i <= 8; boom_i++)
{
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, bomb_width, bomb_high, &img_boom1, boom_i * bomb_width, 0, NOTSRCERASE);//放置飞机爆炸黑白图片
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, bomb_width, bomb_high, &img_boom2, boom_i * bomb_width, 0, SRCINVERT);//放置飞机爆炸的彩色图片
}
score = score + 50;
enemy_plane[k].center_x = rand() % 750 + 34 * k;
enemy_plane[k].center_y = -enemy1_plane_high / 4;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
enemy_plane[k].bullet = rand() % 20;
if (enemy_plane[k].bullet >= 0 && enemy_plane[k].bullet <= 16)
enemy_plane[k].HP = 2;
if (enemy_plane[k].bullet >= 17 && enemy_plane[k].bullet <= 18)
enemy_plane[k].HP = 4;
enemy_plane[k].isFire = rand() % 30;
}
}
else if (enemy_plane[k].bullet >= 17 && enemy_plane[k].bullet <= 18) //打印第二种敌机
{
if (enemy_plane[k].HP > 0)
{
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_enemy2_plane1, NOTSRCERASE);//敌机，黑白
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_enemy2_plane2, SRCINVERT);//敌机，彩色
}
else if (enemy_plane[k].HP <= 0)//敌方飞机HP == 0时爆炸，产生新的敌机
{
for (boom_i = 0; boom_i <= 8; boom_i++)
{
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, bomb_width, bomb_high, &img_boom1, boom_i * bomb_width, 0, NOTSRCERASE);//放置飞机爆炸黑白图片
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, bomb_width, bomb_high, &img_boom2, boom_i * bomb_width, 0, SRCINVERT);//放置飞机爆炸的彩色图片
}
score = score + 100;
enemy_plane[k].center_x = rand() % 750 + 34 * k;
enemy_plane[k].center_y = -enemy1_plane_high / 4;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
enemy_plane[k].bullet = rand() % 20;
if (enemy_plane[k].bullet >= 0 && enemy_plane[k].bullet <= 16)
enemy_plane[k].HP = 2;
if (enemy_plane[k].bullet >= 17 && enemy_plane[k].bullet <= 18)
enemy_plane[k].HP = 4;
enemy_plane[k].isFire = rand() % 30;
}
}
else if (enemy_plane[k].bullet == 19)//打印第三种敌机
{
if (enemy_plane[k].HP > 0)
{
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_enemy3_plane1, NOTSRCERASE);//敌机，黑白
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_enemy3_plane2, SRCINVERT);//敌机，彩色
}
else if (enemy_plane[k].HP <= 0)//敌方飞机HP == 0时爆炸，产生奖励，并继续向下走
{
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_reward1_1, NOTSRCERASE);
putimage(enemy_plane[k].left_x, enemy_plane[k].top_y, &img_reward1_2, SRCINVERT);
}
}
}
printBullet();//打印子弹
}
else if (scene == 2)
{
/*打印boss*/
if (boss.HP > 0)
{
putimage(boss.left_x, boss.top_y, &img_boss1, NOTSRCERASE);
putimage(boss.left_x, boss.top_y, &img_boss2, SRCINVERT);
}
else if (boss.HP <= 0)
{
settextcolor(WHITE);//设置字体为黑色
setbkmode(TRANSPARENT);//背景透明
settextstyle(50, 0, _T("黑体"));
outtextxy(width * 0.4, high * 0.4, "VICTORY");
settextstyle(25, 0, _T("黑体"));
outtextxy(width * 0.7, high * 0.7, "制作人：");
outtextxy(width * 0.7, high * 0.8, "王俊翔");
outtextxy(width * 0.7, high * 0.85, "郭源恒");
}
printBullet();//打印子弹

}
/*显示分数*/
if (scene == 1 || scene == 2 || scene == -1)
{
TCHAR s[5];
char ss[10] = "积分：";
setbkmode(TRANSPARENT);//背景透明
_stprintf_s(s, _T("%d"), score);
outtextxy(20, 12, ss);
outtextxy(100, 12, s);
settextstyle(25, 0, _T("楷体"));
settextcolor(WHITE);
}
/*显示HP*/
if (scene == 1 || scene == 2)
{
putimage(20, 45, HP_i, HP_j, &img_HP, 0, 0);
}
FlushBatchDraw();
Sleep(2);
}


void updateWithoutInput()
{
int i, j;


if (scene == 1)
{
/*我机子弹移动*/
for (int k = 0; k < enemy_planeNum; k++)
{
for (i = 0; i < high; i++)
{
for (j = 0; j < width; j++)
{
if (canvas[i][j] > 0)
{
/*子弹击中敌机*/
if (enemy_plane[k].HP > 0)
{
if (i <= enemy_plane[k].bottom_y && j >= enemy_plane[k].left_x && j <= enemy_plane[k].right_x && my_plane.top_y > enemy_plane[k].bottom_y)
{
enemy_plane[k].HP = enemy_plane[k].HP - my_plane.bullet;
canvas[i][j] = 0;
}
}
/*子弹上移*/
int temp = canvas[i][j];
canvas[i][j] = 0;
if (i > 5)
canvas[i - 3][j] = temp;
}
}
}
}
for (int k = 0; k < enemy_planeNum; k++)
{
enemy_plane[k].isFire = rand() % 80;//敌机随机发射子弹，0为不发射，1为发射
/*敌机发射子弹,且HP>0*/
if (enemy_plane[k].HP >= 0 && enemy_plane[k].bottom_y > 15)
{
if (enemy_plane[k].isFire == 1)
{
int m, n;
m = enemy_plane[k].bottom_y;
n = enemy_plane[k].bottom_x;
if (enemy_plane[k].bullet >= 0 && enemy_plane[k].bullet <= 16)
canvas[m][n] = -1;
if (enemy_plane[k].bullet >= 17 && enemy_plane[k].bullet <= 18)
canvas[m][n] = -2;
}
}
}


/*敌机子弹移动*/
for (i = high - 1; i > 0; i--)
{
for (j = width - 1; j > 0; j--)
{
/*子弹下移*/
if (canvas[i][j] < 0)
{
/*子弹击中我机*/
if (abs(my_plane.center_y - i) <= my_plane_high / 2 && j >= my_plane.left_x && j <= my_plane.right_x && my_plane.HP > 0)
{
if (canvas[i][j] == -1)
{
my_plane.HP = my_plane.HP - 1;//敌机1击中我机HP-1
HP_i = HP_i - 40;


}
if (canvas[i][j] == -2)
{
my_plane.HP = my_plane.HP - 2;//敌机2击中我机HP-2
HP_i = HP_i - 80;
}
canvas[i][j] = 0;
mybullet = 0;
my_plane.bullet = 1;
}
/*子弹下移*/
int temp = canvas[i][j];
canvas[i][j] = 0;
if (i < high - 2)
canvas[i + 4][j] = temp;
}
}
}
/*敌机撞到我机*/
for (int k = 0; k < enemy_planeNum; k++)
{
if (abs(my_plane.center_y - enemy_plane[k].center_y) <= my_plane_high / 2 + enemy1_plane_high / 2 - 20 && enemy_plane[k].HP > 0)
{
if (enemy_plane[k].left_x <= my_plane.right_x && enemy_plane[k].right_x >= my_plane.left_x && my_plane.HP > 0)
my_plane.HP = 0;
}
/*敌机向下移动*/
if (enemy_plane[k].HP > 0)
{
if (enemy_plane[k].top_y <= high)
{
enemy_plane[k].center_y = enemy_plane[k].center_y + 1.5;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
}
/*敌机飞出屏幕，重置新的敌机*/
else
{
enemy_plane[k].center_x = rand() % 750 + 34 * k;
enemy_plane[k].center_y = -enemy1_plane_high / 4;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
enemy_plane[k].bullet = rand() % 20;
if (enemy_plane[k].bullet >= 0 && enemy_plane[k].bullet <= 16)
enemy_plane[k].HP = 2;
if (enemy_plane[k].bullet >= 17 && enemy_plane[k].bullet <= 18)
enemy_plane[k].HP = 4;
enemy_plane[k].isFire = rand() % 30;
}
}
/*当敌机为第三种时，打爆生成奖励，继续往下飞行，与我机坐标重合后可子弹威力+1*/
if (enemy_plane[k].HP <= 0 && enemy_plane[k].bullet == 19)
{
/*判断我机是否吃掉奖励，若吃掉则生成新的飞机*/
if (abs(enemy_plane[k].center_y - my_plane.center_y) < my_plane_high / 2 + reward_high / 2)
{
if (enemy_plane[k].left_x <= my_plane.right_x && enemy_plane[k].right_x >= my_plane.left_x && my_plane.HP > 0)
{
if (my_plane.bullet < 2)
my_plane.bullet++;
mybullet = 1;
enemy_plane[k].center_x = rand() % 750 + 34 * k;
enemy_plane[k].center_y = -enemy1_plane_high / 4;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
enemy_plane[k].bullet = rand() % 20;
if (enemy_plane[k].bullet >= 0 && enemy_plane[k].bullet <= 16)
enemy_plane[k].HP = 2;
if (enemy_plane[k].bullet >= 17 && enemy_plane[k].bullet <= 18)
enemy_plane[k].HP = 4;
if (enemy_plane[k].bullet == 19)
enemy_plane[k].HP = 1;
enemy_plane[k].isFire = rand() % 30;
}
}
/*判断奖励是否下移，若已经到头则生成新的飞机*/
if (enemy_plane[k].top_y <= high)
{
enemy_plane[k].center_y = enemy_plane[k].center_y + 1.5;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
}
else
{
enemy_plane[k].center_x = rand() % 750 + 34 * k;
enemy_plane[k].center_y = -enemy1_plane_high / 4;
enemy_plane[k].left_x = enemy_plane[k].center_x - enemy1_plane_width / 2;
enemy_plane[k].left_y = enemy_plane[k].center_y;
enemy_plane[k].right_x = enemy_plane[k].center_x + enemy1_plane_width / 2;
enemy_plane[k].right_y = enemy_plane[k].center_y;
enemy_plane[k].top_x = enemy_plane[k].center_x;
enemy_plane[k].top_y = enemy_plane[k].center_y - enemy1_plane_high / 2;
enemy_plane[k].bottom_x = enemy_plane[k].center_x;
enemy_plane[k].bottom_y = enemy_plane[k].center_y + enemy1_plane_high / 2;
enemy_plane[k].bullet = rand() % 20;
if (enemy_plane[k].bullet >= 0 && enemy_plane[k].bullet <= 16)
enemy_plane[k].HP = 2;
if (enemy_plane[k].bullet >= 17 && enemy_plane[k].bullet <= 18)
enemy_plane[k].HP = 4;
if (enemy_plane[k].bullet == 19)
enemy_plane[k].HP = 1;
enemy_plane[k].isFire = rand() % 30;
}
}
}
}
else if (scene == 2)
{
/*我机发射子弹*/
for (i = 0; i < high; i++)
{
for (j = 0; j < width; j++)
{
if (canvas[i][j] > 0)
{
if (boss.HP > 0)//子弹击中boss
{
if (i <= boss.bottom_y && j >= boss.left_x && j <= boss.right_x && my_plane.top_y > boss.bottom_y)
{
boss.HP = boss.HP - my_plane.bullet;
canvas[i][j] = 0;
}
}
/*子弹上移*/
int temp = canvas[i][j];
canvas[i][j] = 0;
if (i > 5)
canvas[i - 3][j] = temp;
}
}
}
/*boss发射子弹*/
if (boss.HP > 0)
{
if (boss.bullet < 250)
{
if (boss.isFire == 1)
{
int m1, n1, m2, n2, m3, n3;
m1 = boss.bottom_y;
n1 = boss.bottom_x;
m2 = boss.right_y;
n2 = boss.right_x;
m3 = boss.left_y;
n3 = boss.left_x;
canvas[m1][n1] = -3;
canvas[m2][n2] = -4;
canvas[m3][n3] = -5;
}
boss.isFire++;
if (boss.isFire == 20)
boss.isFire = 1;


//boss子弹移动
for (i = high - 1; i > 0; i--)
{
for (j = width - 1; j > 0; j--)
{
//*子弹下移
if (canvas[i][j] <= -3 && canvas[i][j] >= -5)
{
//*子弹击中我机
if (abs(my_plane.center_y - i) <= my_plane_high / 2 && j >= my_plane.left_x && j <= my_plane.right_x && my_plane.HP > 0)
{
my_plane.HP = my_plane.HP - 1;//boss击中我机HP-3
HP_i = HP_i - 40;
canvas[i][j] = 0;
mybullet = 0;  //变为我机第一类子弹
my_plane.bullet = 1;//伤害变为1
}
//子弹下移
if (canvas[i][j] == -3)
{
int temp = canvas[i][j];
canvas[i][j] = 0;
if (i < high - 2)
canvas[i + 2][j] = temp;
}
if (canvas[i][j] == -4)
{
int temp = canvas[i][j];
canvas[i][j] = 0;
if (i < high - 2 && j < width - 5)
canvas[i + 2][j + 2] = temp;
}
if (canvas[i][j] == -5)
{
int temp = canvas[i][j];
canvas[i][j] = 0;
if (i < high - 2 && j > 5)
canvas[i + 2][j - 2] = temp;
}
}
}
}
}


if (boss.bullet >= 300 && boss.bullet <= 650)
{


if (boss.isFire == 1)
{
int n;
n = boss.bottom_y;
for (int m = boss.bottom_x - boss_width / 2; m<boss.bottom_x + boss_width / 2; m = m + 20)
{
canvas[n][m] = -6;
}
}
boss.isFire++;
if (boss.isFire == 15)
boss.isFire = 1;
//boss子弹移动
for (i = high - 1; i > 0; i--)
{
for (j = width - 1; j > 0; j--)
{
//*子弹下移
if (canvas[i][j] == -6)
{
//*子弹击中我机
if (abs(my_plane.center_y - i) <= my_plane_high / 2 && j >= my_plane.left_x && j <= my_plane.right_x && my_plane.HP > 0)
{
my_plane.HP = my_plane.HP - 3;//boss击中我机HP-3
HP_i = HP_i - 40;
canvas[i][j] = 0;
mybullet = 0;  //变为我机第一类子弹
my_plane.bullet = 1;//伤害变为1
}
//子弹下移
int temp = canvas[i][j];
canvas[i][j] = 0;
if (i < high - 2)
canvas[i + 2][j] = temp;
}
}
}
}


boss.bullet++;
if (boss.bullet == 250)
{
for (i = high - 1; i > 0; i--)
for (j = width - 1; j > 0; j--)
canvas[i][j] = 0;
}
if (boss.bullet == 650)
{
for (i = high - 1; i > 0; i--)
for (j = width - 1; j > 0; j--)
canvas[i][j] = 0;


}
if (boss.bullet == 750)
boss.bullet = 1;
/*boss撞到我机*/
if (boss.bottom_y < high - 10)
{
//boss向下移动
boss.center_y = boss.center_y + 0.1;
boss.left_x = boss.center_x - boss_width / 2;
boss.left_y = boss.center_y;
boss.right_x = boss.center_x + boss_width / 2;
boss.right_y = boss.center_y;
boss.top_x = boss.center_x;
boss.top_y = boss.center_y - boss_high / 2;
boss.bottom_x = boss.center_x;
boss.bottom_y = boss.center_y + boss_high / 2;
}
if (abs(my_plane.center_y - boss.center_y) < my_plane_high / 2 + boss_high / 2 - 20)
{
if (boss.left_x <= my_plane.right_x && boss.right_x >= my_plane.left_x && boss.HP > 0)
my_plane.HP = 0;
}


if (boss.bottom_y >= high - 15)
{
my_plane.HP = 0;
}
}
else if (boss.HP <= 0)
{
mciSendString("close bossbomb", NULL, 0, NULL);
mciSendString("open .\\showbomb.mp3 alias bossbomb", NULL, 0, NULL);
mciSendString("play bossbomb", NULL, 0, NULL);
for (i = 0; i < high; i++)
{
for (j = 0; j < width; j++)
{
canvas[i][j] = 0;
}
}
}
}
}


void updateWithInput()
{
MOUSEMSG m;
while (MouseHit())
{
m = GetMouseMsg();
/*飞机的位置等于鼠标所在的位置*/
if (m.uMsg == WM_MOUSEMOVE && my_plane.HP > 0)
{
my_plane.center_x = m.x;
my_plane.center_y = m.y;
my_plane.left_x = my_plane.center_x - my_plane_width / 2;
my_plane.left_y = my_plane.center_y;
my_plane.right_x = my_plane.center_x + my_plane_width / 2;
my_plane.right_y = my_plane.center_y;
my_plane.top_x = my_plane.center_x;
my_plane.top_y = my_plane.center_y - my_plane_high / 2;
my_plane.bottom_x = my_plane.center_x;
my_plane.bottom_y = my_plane.center_y + my_plane_high / 2;
}
/*发射子弹*/
else if (m.uMsg == WM_LBUTTONDOWN && my_plane.HP > 0 && my_plane.top_y >10 && mybullet == 0)
{
int m, n;
m = my_plane.top_y;
n = my_plane.top_x;
canvas[m][n] = 1;//canvas数组中1代表我方1号子弹
mciSendString("close planeshoot1", NULL, 0, NULL);//关闭上一次打开的音乐
mciSendString("open .\\planeshoot1.mp3 alias planeshoot1", NULL, 0, NULL);//打开音乐
mciSendString("play planeshoot1", NULL, 0, NULL);//播放一次
}
else if (m.uMsg == WM_LBUTTONDOWN && my_plane.HP > 0 && my_plane.top_y >10 && mybullet == 1)
{
int m, n;
m = my_plane.top_y;
n = my_plane.top_x;
canvas[m][n] = 2;//canvas数组中2代表我方2号子弹
mciSendString("close planeshoot2", NULL, 0, NULL);//关闭上一次打开的音乐
mciSendString("open .\\planeshoot2.mp3 alias planeshoot2", NULL, 0, NULL);//打开音乐
mciSendString("play planeshoot2", NULL, 0, NULL);//播放一次
}
}
char input;  
if (scene == 1 || scene == 2)//第一关或第二关
{
if (GetAsyncKeyState(0x1B) & 0x8000 && (scene == 1 || scene == 2))//游戏暂停按esc键
{
scene = -1;//游戏暂停界面
}
}
}


void startMenu()
{
putimage(0, 0, &img_startMenu);
settextcolor(BLACK);//设置字体为黑色
setbkmode(TRANSPARENT);//背景透明
settextstyle(50, 0, _T("黑体"));
outtextxy(width * 0.4, high * 0.4, "开始游戏");
outtextxy(width * 0.4, high * 0.6, "退出游戏");
/*点击开始游戏进入游戏，点击退出游戏退出*/
MOUSEMSG m;
if (MouseHit())
{
m = GetMouseMsg();
if (m.uMsg == WM_LBUTTONUP && scene == -2)
{
if (m.x <= 585 && m.x >= 360 && m.y <= 338 && m.y >= 270)//按键范围判断
{
scene = 1;
}
if (m.x <= 585 && m.x >= 360 && m.y <= 472 && m.y >= 405)//按键范围判断
{
exit(0);
}
}
}
FlushBatchDraw();
}


void pauseMenu()
{
putimage(0, 0, &img_pauseMenu);
settextcolor(BLACK);//设置字体为黑色
setbkmode(TRANSPARENT);//背景透明
settextstyle(50, 0, _T("黑体"));
outtextxy(width * 0.4, high * 0.4, "继续游戏");
outtextxy(width * 0.4, high * 0.6, "退出游戏");
/*点击开始游戏进入游戏，点击退出游戏退出*/
MOUSEMSG m;
if (MouseHit())
{
m = GetMouseMsg();
if (m.uMsg == WM_LBUTTONUP && scene == -1)
{
if (m.x <= 585 && m.x >= 360 && m.y <= 338 && m.y >= 270)//按键范围判断
{
scene = 1;
}
if (m.x <= 585 && m.x >= 360 && m.y <= 472 && m.y >= 405)//按键范围判断
{
exit(0);
}
}
}
FlushBatchDraw();
}
void gameover()
{
EndBatchDraw();
_getch();
closegraph();
}

