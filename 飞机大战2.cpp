# include<stdio.h>
# include<conio.h>
# include<graphics.h>
# include<math.h>
# pragma comment(lib,"Winmm.lib")
# define High 500
# define Width 800
IMAGE img_bk;
float position_x, position_y;
float bullet_x, bullet_y;
float enemy_x, enemy_y;
IMAGE c, img_planeNormal2;
IMAGE img_planeExplode1, img_planeExplode2;
IMAGE img_bullet1, img_bullet2;
IMAGE img_enemyPlane1, img_enemyPlane2;
int isExpolde = 0;
int score = 0;
void startup()
{
	mciSendString("open D:\\game_music.mp3 alias bkmusic", NULL, 0, NULL);
	mciSendString("play bkmusic repeat", NULL, 0, NULL);
	initgraph(Width, High);
	loadimage(&img_bk, "D:\\background.jpg");
	
	loadimage(&img_planeNormal2, "D:\\planeNormal_2.jpg");
	loadimage(&img_bullet1, "D:\\bullet1.jpg");
	loadimage(&img_bullet2, "D:\\bullet2.jpg");
	loadimage(&img_enemyPlane1, "D:\\enemyPlane1.jpg");
	loadimage(&img_enemyPlane2, "D:\\enemyPlane2.jpg");
	loadimage(&img_planeExplode1, "D:\\planeExplode_1.jpg");
	loadimage(&img_planeExplode2, "D:\\planeExplode_2.jpg");
	position_x = Width * 0.5;
	position_y = High * 0.7;
	bullet_x = position_x;
	bullet_y = -85;
	enemy_x = Width * 0.5;
	enemy_y = 10;
	BeginBatchDraw();
}
void show()
{
	putimage(0, 0, &img_bk);
	if (isExpolde == 0)
	{
		
		putimage(position_x - 50, position_y - 60, &img_planeNormal2, SRCINVERT);
		putimage(bullet_x - 7, bullet_y, &img_bullet1, NOTSRCERASE);
		putimage(bullet_x - 7, bullet_y, &img_bullet2, SRCINVERT);
		putimage(enemy_x, enemy_y, &img_enemyPlane1, NOTSRCERASE);
		putimage(enemy_x, enemy_y, &img_enemyPlane2, SRCINVERT);
	}
	else
	{
		putimage(position_x - 50, position_y - 60, &img_planeExplode1, NOTSRCERASE);
		putimage(position_x - 50, position_y - 60, &img_planeExplode1, SRCINVERT);
	}
	outtextxy(Width * 0.48, High * 0.95, "�÷�:");
	char s[5];
	sprintf(s, "%d", score);
	outtextxy(Width * 0.55, High * 0.95, s);
	FlushBatchDraw();
	Sleep(2);
}
void updateWithoutInput()
{
	if (isExpolde == 0)
	{
		if (bullet_y > -25)
			bullet_y = bullet_y - 2;
		if (enemy_y < High - 25)
			enemy_y = enemy_y + 0.5;
		else
			enemy_y = 10;
		if (fabs(bullet_x - enemy_x) + fabs(bullet_y - enemy_y) < 80)
		{
			enemy_x = rand() % Width;
			enemy_y = -40;
			bullet_y = -85;
			mciSendString("close gemusic", NULL, 0, NULL);
			mciSendString("open D:\\gotEnemy.mp3 alias gemusic", NULL, 0, NULL);
			mciSendString("play gemusic", NULL, 0, NULL);
			score++;
			if (score > 0 && score % 5 == 0 && score % 2 != 0)
			{
				mciSendString("close 5music", NULL, 0, NULL);
				mciSendString("open D:\\5.mp3 alias 5music", NULL, 0, NULL);
				mciSendString("play 5music", NULL, 0, NULL);
			}
			if (score % 10 == 0)
			{
				mciSendString("close 10music", NULL, 0, NULL);
				mciSendString("open D:\\10.mp3 alias 5music", NULL, 0, NULL);
				mciSendString("play 10music", NULL, 0, NULL);
			}
		}
		if (fabs(position_x - enemy_x) + fabs(position_y - enemy_y) < 150)
		{
			isExpolde = 1;
			mciSendString("close exmusic", NULL, 0, NULL);
			mciSendString("open D:\\explode.mp3 alias exmusic", NULL, 0, NULL);
			mciSendString("play exmusic", NULL, 0, NULL);
		}
	}
}
void updateWithInput()
{
	if (isExpolde == 0)
	{
		MOUSEMSG m;
		while (1)
		{
			m = GetMouseMsg();
			if (m.uMsg == WM_MOUSEMOVE)
			{
				position_x = m.x;
				position_y = m.y;
			}
			else if (m.uMsg == WM_MOUSEMOVE)
			{
				bullet_x = position_x;
				bullet_y = position_y - 85;
				mciSendString("close fgmusic", NULL, 0, NULL);
				mciSendString("open D:\\f_gun.mp3 alias fgmusic", NULL, 0, NULL);
				mciSendString("play fgmusic", NULL, 0, NULL);
			}
		}
	}
}
void gameover()
{
	EndBatchDraw();
	_getch();
	closegraph();

}
int main()
{
	startup();
	while (1)
	{
		show();
		updateWithoutInput();
			updateWithInput();
	}
	gameover();
	return 0;
}

