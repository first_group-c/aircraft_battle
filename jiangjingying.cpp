#include<graphics.h>
#include<conio.h>

#pragma comment(lib,"Winmm.lib")
#define High 864
#define Width 591

IMAGE img_bk;
int position_x,position_y;
IMAGE img_planeNormal1,img_planeNormal2;

void startup()
{
	initgraph(Width,High);
	loadimage(&img_bk,"C:\\Users\\Administrator\\Pictures\\background.jpg");
	loadimage(&img_planeNormal1,"C:\\Users\\Administrator\\Pictures\\planeNormal_1.jpg");
	loadimage(&img_planeNormal2,"C:\\Users\\Administrator\\Pictures\\planeNormal_2.jpg");
	position_x=High*0.7;
	position_y=Width*0.5;
	BeginBatchDraw();
 } 
 
 void show()
 {
 	putimage(0,0,&img_bk);
 	putimage(position_x-50,position_y-60,&img_planeNormal1,NOTSRCERASE);
 	putimage(position_x-50,position_y-60,&img_planeNormal2,SRCINVERT);
 	FlushBatchDraw();
 	Sleep(2);
 }
 
 void updateWithoutInput()
 {
 	
 }
 
 void updateWithInput()
 {
 	MOUSEMSG m;
 	while(MouseHit())
 	{
 		m = GetMouseMsg();
 		{
 			position_x = m.x;
 			position_y = m.y;
		 }
	 }
 }
 
 void gameover()
 {
 	EndBatchDraw();
 	getch();
 	closegraph();
 }
 
 int main()
 {
 	startup();
 	while(1)
 	{
 		show();
 		updateWithoutInput();
 		updateWithInput();
	 }
	 gameover();
	 return 0;
 }

