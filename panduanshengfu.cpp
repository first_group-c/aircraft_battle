#include<graphics.h>
#include<conio.h>
#include<math.h>

#pragma comment(lib,"Winmm.lib")

#define High 600
#define Width 600

IMAGE img_bk;
float postion_x,position_y;
float bullet_x,bullet_y;
float enemy_x,enemy_y;
IMAGE img_planeNormall,img_planeNorma12;
IMAGE img_planeExplode1,img_planeExplode2;
IMAGE img_bullet1,img_bullet2;
IMAGE img_enemyPlane1,img_enemyPlane2;
int isExpode=0;

void starup()
{
	initgraph(Width,High);
	loadimage(&img_bk,"D:\\background.jpg");
    loadimage(&img_planeNormal1,"D:\\planeNormal_1.jpg");
    loadimage(&img_planeNormal2,"D:\\planeDorml_2.jpg");
    loadimage(&img_bullet1,"D:\\bullet1.jpg");
    loadimage(&img_bullet2,"D:\\bullet2.jpg");
    loadimage(&img_enemyPlane1,"D:\\enemyPlane1.jpg");
    loadimage(&img_enemyPlane2,"D:\\enemyPlane2.jpg");
    loadimage(&img_planeExplode1,"D:\\planeExplode_1.jpg");
    loadimage(&img_planeExplode2,"D:\\planeExplode_2.jpg");
    position_x=Width*0.5;
    position_y=High*0.7;
    bullet_x=position_x;
	bullet_y=-85;
	enemy_x=Width*0.5;
	enemy_y=10;
	BeginBatchDraw(); 
}

void show()
{
	putimage(0,0,&img_bk);
	if(isExplode==0)
	{
		putimage(position_x-50,position_y-60,&img_planeNormal1,NOTSRCERASE);
		
		putimage(position_x-50,position_y-60,&img_planeNormal2,SRCINVERT);
		
		putimage(bullet_x-7,bullet_y,&img_bullet1,NOTSRCERASE);
		putimage(bullet_x-7,bullet_y,&img_bullet2,SRCINVERT);
		putimage(enemy_x,enemy_y,&img_enemyPlane1,NOTSRCERASE);
		putimage(enemy_x,enemy_y,&img_enemyPlane2,SRCINVERT);
	}
	else
	{
		putimage(position_x-50,position_y-60,&img_planeExplode1,NOTSRCERASE);
		
		putimage(position_x-50,position_y-60,&img_planeExplode2,SRCINVERT);
	}
	FlushBatchDraw();
	Sleep(2);
}

void updateWithoutInput()
{
	if(bullet_y>-25)
	bullet_y=bullet_y-2;
	if(enemy_y<High-25)
	enemy_y=enemy_y+0.5;
	else
	enemy_y=10;
	if(abs(bullet_x-enemy_x)+abs(bullet_y-enemy-y)<50)
	{
		enemy_x=rand()%Width;
		enemy_y=-40;
		bullet_y=-85;
	}
	if(abs(position_x-enemy_x)+abs(position_y-enemy_y)<150)
	isExplode=1;
}
void updateWithInput()
{
	MOUSEMSG m;
	while(MouseHit())
	{
		m=GetMouseMsg();
		if(m.uMsg==WM_MOUSEMOVE)
		{
			position_x=m.x;
			position_y=m.y;
		}
		else if(m.uMsg==WM_LBUTTONDOWN)
		{
			bullet_x=position_x;
			bullet_y=position_y-85;
		}
	}
}
void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

int main()
{
	starup();
	while(1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	gameover();
	return 0;
}
