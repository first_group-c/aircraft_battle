#include <stdio.h>
#include<time.h>
#include <stdlib.h>
#include <conio.h>//通用输入输出库，主要是文件和标准控制台的输入输出
#include <windows.h>//包含一会要使用的Sleep()函数等 



void hide_cursor(void){
	CONSOLE_CURSOR_INFO info={1,0};
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&info);
}

int main(void){
	int x=5;
	int y=10;
	int i,j;
	int is_fire=0;//是否发射 
	int nx=y/2;//靶子位置 
	int is_killed=0;//是否被干掉 
	 
	srand(time(NULL));//产生随机数种子 
	hide_cursor();//隐藏光标 
	system("mode con cols=60 lines=40");//设置屏幕为手机格式 
	while(1){
		system("cls");
		
		if(is_killed==0){//没有被干掉，绘制靶子
			for(i=0;i<nx;i++) 
			printf(" ");
			printf("@\n");
		}
		
		if(is_fire==0){
			for(i=0;i<y;i++) printf("\n");
		}else{
			for(i=0;i<y;i++){
				for(j=0;j<x;j++)
					printf(" ");
				printf("  |\n");
				is_fire=0;
			}
			
			if(nx==x+2)
				is_killed=1;
		}
		
		
		//绘制飞机 
		for(i=0;i<x;i++) printf(" ");
		printf("  *\n");
		for(i=0;i<x;i++) printf(" ");
		printf("*****\n");
		for(i=0;i<x;i++) printf(" ");
		printf(" * *\n");
	
		
		if( kbhit() ){  //如果检测到键盘按键，返回1 
			int c= getch();  //获取键盘值，不需要按回车 
			switch(c){
				case 'a':x--;break;
				case 'd':x++;break;
				case 's':y++;break;
				case 'w':y--;break;
				case ' ':is_fire=1;break;
			}
		}
		Sleep(200); //滞留200毫秒  有停顿的输出 
	}
}

